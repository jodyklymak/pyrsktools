MAIN_SOURCE := pyrsktools/__init__.py \
               pyrsktools/channeltypes.py
TEST_SOURCE := tests/test.py \
               tests/context.py

package: source
	python3 setup.py bdist_wheel

install: source
	pip3 install -e .[NumPy]

source: ${MAIN_SOURCE} \
        setup.py \
        MANIFEST.in \
        README.rst \
        CHANGES.rst \
        LICENSE.txt

upload: package
	twine upload dist/pyRSKTools-*-py3-none-any.whl

test: ${MAIN_SOURCE} ${TEST_SOURCE}
	python3 -m unittest discover -v tests/

lint: ${MAIN_SOURCE} ${TEST_SOURCE}
	pyflakes $^

.PHONY: clean
clean:
	# Clean up build artifacts.
	-rm -R build dist pyRSKTools.egg-info
	# Clean up bytecode leftovers.
	find . -type f -name '*.pyc' -print0 | xargs -0 rm
	find . -type d -name '__pycache__' -print0 | xargs -0 rmdir
