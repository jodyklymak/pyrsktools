=======
Testing
=======

A clean bill of health
from both unit tests and the linter
is required for a successful automated build.

Unit Tests
==========

To test the project,
set the ``RSK`` environment variable
to point at an RSK file:

.. code-block:: shell

    $ export RSK=~/Downloads/some_rsk.rsk

Then run
the unit tests:

.. code-block:: shell

    $ make test

Linting
=======

To analyze the project for errors
with Pyflakes_,
use the ``lint`` make target:

.. code-block:: shell

    $ make lint

.. _Pyflakes: https://pypi.org/project/pyflakes/
