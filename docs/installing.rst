==========
Installing
==========

PyPI should have
a reasonably up-to-date version available:

.. code-block:: shell

    $ pip3 install pyrsktools

If you want to install
the latest development version
from source,
you can do that too:

.. code-block:: shell

    $ git clone https://bitbucket.org/rbr/pyrsktools
    $ cd pyrsktools
    $ make install
    $ # Or, if you prefer to do it yourself...
    $ pip3 install -e .
