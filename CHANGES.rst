=========
Changelog
=========

v0.1.7
======

Released 2018-06-29.

Added
-----

- Use Pyflakes for linting.

Fixed
-----

- The UTC timezone is now embedded in ``datetime`` instances
  produced by the library.
  v0.1.6 interpreted timestamps from the dataset as UTC,
  but didn't embed the timezone into the ``datetime`` instance,
  so the values did not round-trip back into the same timestamps.
  This made region-based queries
  (for profiles and casts)
  inaccurate.

v0.1.6
======

Released 2018-05-24.

Added
-----

- This changelog 😉

Changed
-------

- The ``RSK.channels`` dictionary
  is now ordered.

- The format of automatically-generated channel labels
  has changed to match
  the format of channel labels
  stored in RBR Generation3 instruments
  (e.g., “conductivity_00” instead of “conductivity”).
  This is a very breaking change:
  all channel labels will have changed,
  which will break any field access
  to samples.

- The ``Channel.label`` field has been added
  to store this label
  in addition to the use of labels
  as ``Sample`` field names
  and keys on the ``RSK.channels`` dictionary.

- The ``Channel.short_name`` field
  has been renamed to ``Channel.key``
  to reflect nomenclature used everywhere
  outside of the dataset schema.

Fixed
-----

- Dataset timestamps
  should be interpreted as UTC.
  (Thanks to Gunnar Voet.)
- Datasets with multiple identically-named channels
  can be opened correctly.
- Older datasets
  which did not have the ``region.description`` column
  can now be opened correctly.
